package com.epam.rd.june2018.maven_multimodule_example.echo.services;

public class SystemOutEchoService implements EchoService {

    @Override
    public void echo(String str) {
        System.out.println(str);
    }
}
