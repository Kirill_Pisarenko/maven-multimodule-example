package com.epam.rd.june2018.maven_multimodule_example.echo.services;

public interface EchoService {
    void echo(String str);
}
