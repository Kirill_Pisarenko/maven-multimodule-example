# maven-multimodule-example

## This program prints Command-Line Arguments

* To build project execute in cmd: 
> mvn clean install

* To run application execute in cmd:

// success scenario
> java -jar .\console\target\echo-jar-with-dependencies.jar ololo!

// fail scenario
> java -jar .\console\target\echo-jar-with-dependencies.jar